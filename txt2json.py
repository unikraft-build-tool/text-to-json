#!/usr/bin/python3


import getopt
import json
import sys



if __name__ == "__main__":
    optlist, args = getopt.getopt(sys.argv[1:], "s:i:r:e")
    separator = " "
    infile = sys.stdin
    remove_empty = False
    remove_on_word = []
    for opt in optlist:
        if opt[0] == "-i":
            infile = open(opt[1], "r")
        if opt[0] == "-s":
            separator = opt[1]
        if opt[0] == "-e":
            remove_empty = True
        if opt[0] == "-r":
            remove_on_word.append(opt[1])
    #print(f"separator={separator} ok")
    data = infile.read()
    split_data = data.split(separator)
    for i in range(len(split_data)):
        split_data[i] = split_data[i].strip()
    if remove_empty:
        split_data = [datum for datum in split_data if not len(datum) == 0 and datum not in remove_on_word]
    print(json.dumps(split_data))


