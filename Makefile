
SHELL := /bin/bash
TESTS_TXT := $(wildcard tests/*.txt)
TESTS_CR_TXT := $(wildcard tests/*.cr)
TESTS_CR_NONE := $(wildcard tests/*.cr.none)

.PHONY: test $(TESTS_TXT) $(TESTS_CR_TXT)


test: $(TESTS_CR_NONE) $(TESTS_CR_TXT) $(TESTS_TXT) txt2json.py

$(TESTS_TXT): %.txt : %.json
	@./txt2json.py -i $@ -e | jq | cmp -s $^ || (echo test $@ failed && echo "input:" && cat $@ && echo "output:" && ./txt2json.py -i $@ | jq && echo "expected:" && cat $^ &&  exit 1)
	@echo $@ passed

$(TESTS_CR_TXT): %.cr : %.json
	@./txt2json.py -i $@ -s $$'\n' -e | jq | cmp -s $^ || (echo test $@ failed && echo "input:" && cat $@ && echo "output:" && ./txt2json.py -i $@ -e -s $$'\n' | jq  && echo "expected:" && cat $^ &&  exit 1)
	@echo $@ passed

$(TESTS_CR_NONE): %.cr.none : %.json
	@./txt2json.py -i $@ -s $$'\n' -e -r none | jq | cmp -s $^ || (echo test $@ failed && echo "input:" && cat $@ && echo "output:" && ./txt2json.py -i $@ -s $$'\n' -e -r none | jq && echo "expected:" && cat $^ && exit 1)
	@echo $@ passed
